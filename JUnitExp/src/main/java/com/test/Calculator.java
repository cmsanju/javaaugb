package com.test;

import java.util.Arrays;
import java.util.List;

public class Calculator {
	
	public int add(int x, int y)
	{
		return x+y;
	}
	
	public int mul(int x, int y)
	{
		return x*y;
	}
	
	public int sub(int x, int y)
	{
		return x-y;
	}
	
	public String greet(String msg)
	{
		return msg;
	}
	
	public List<String> userNames(List<String> names)
	{
		//List<String> names = Arrays.asList("Hero1", "Hero2", "Hero3", "Hero4");
		
		return names;
	}

	public String greet1(String msg)
	{
		return msg;
	}

	public String greet2(String msg)
	{
		return msg;
	}

}
