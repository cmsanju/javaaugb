package com.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class TestCalculator {
	
	Calculator obj;
	
	@BeforeAll
	public static void beforeAll()
	{
		System.out.println("Before all");
	}
	
	@AfterAll
	public static void afterAll()
	{
		System.out.println("After all");
	}
	
	@BeforeEach
	public void setUp()
	{
		System.out.println("before test method");
		
		obj = new Calculator();
		
		//obj = Mockito.mock(Calculator.class);
	}
	
	@AfterEach
	public void setDown()
	{
		System.out.println("after test method");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("ADD TEST METHOD");
		assertEquals(60, obj.add(30, 30));
		
		
	}
	
	@Test
	public void testMul()
	{
		System.out.println("MUL TEST METHOD");
		assertEquals(80, obj.mul(40, 2));
		
		
	}
	@Test
	public void testSub()
	{
		System.out.println("TEST SUB METHOD");
		assertEquals(20, obj.sub(100, 80));
		
	}
	@Test
	public void testGreet()
	{
		System.out.println("TEST GREET METHOD");
		assertEquals("Hi Hello", obj.greet("Hi Hello"));
	}
	@Test
	public void testNameCount()
	{
		List<String> names = Arrays.asList("Hero1", "Hero2", "Hero3", "Hero4");
		
		assertEquals(4, obj.userNames(names).size());
	}

}
